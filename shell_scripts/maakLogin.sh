#!/usr/bin/env bash

password_file=$1

echo "---New Account---"

echo Enter username:
read username

if [ "$username" = "" ]; then
	username=$(whoami)
fi

read -p "Enter password:"  password
read -p "Confirm password:" tmp


while [ $tmp != $password -o ${#password} -lt 8 ]; do

	if ((${#password} < 8)) && (($tmp != $password)); then
		echo Passwords do not match and password must be at least 8 characters long
		read -p "Enter password:"  password
		read -p "Confirm password:" tmp
	elif ((${#password} >= 8)) && (($tmp != $password)); then
		echo Passwords do not match.
		read -p "Enter password:"  password
		read -p "Confirm password:" tmp 
	elif ((${#password} < 8)) && (($tmp == $password)); then
		echo Password must be at least 8 characters long
		read -p "Enter password:"  password
		read -p "Confirm password:" tmp
	fi
done

[ ! -f $password_file ] && touch $password_file
echo $password | md5sum > $password_file
