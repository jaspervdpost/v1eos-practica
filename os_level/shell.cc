#include "shell.hh"
#include <climits>
int main()
{ std::string input;
    std::string prompt;
    int fd = syscall(SYS_open, "prompt.txt", O_RDONLY, 0755);
    char byte[1];
    while(syscall(SYS_read, fd, byte, 1)){
        prompt.append(byte); }


    while(true)
    { std::cout << prompt;                   // Print het prompt
        std::getline(std::cin, input);         // Lees een regel
        if (input == "new_file") new_file();   // Kies de functie
        else if (input == "ls") list();        //   op basis van
        else if (input == "src") src();        //   de invoer
        else if (input == "find") find();
        else if (input == "seek") seek();
        else if (input == "exit") return 0;
        else if (input == "quit") return 0;
        else if (input == "error") return 1;

        if (std::cin.eof()) return 0; } }      // EOF is een exit

        void new_file(){ // ToDo: Implementeer volgens specificatie.
            const char* bestands_naam[] = {NULL};
            const char* invoer[] = {NULL};
            std::string tmp_naam;
            std::string tmp_invoer;
            std::string text_invoer;
            std::cout << "Geef de gewenste bestandsnaam:" << std::endl;
            std::getline(std::cin, tmp_naam);
            bestands_naam[0] = tmp_naam.c_str();
            std::cout << "Geef een invoer voor in het bestand:" << std::endl;
            std::string tmp_tmp_invoer;
            while (std::getline(std::cin, tmp_invoer)){
                if (tmp_invoer == "<EOF>"){
                    break;
                }
                tmp_tmp_invoer += tmp_invoer + "\n";
            }
            invoer[0] = tmp_tmp_invoer.c_str();
            int fd = syscall(SYS_creat, bestands_naam[0], 0644);
            syscall(SYS_write, fd, invoer[0], tmp_tmp_invoer.size());
        }

void list() // ToDo: Implementeer volgens specificatie.
{ // std::cout << "ToDo: Implementeer SYS_fork en SYS_wait" << std::endl;
    int pid = syscall(SYS_fork);
    if (pid == 0){
        const char* flags[] = {"/bin/ls", "-l", "-a", NULL};
        syscall(SYS_execve, flags[0], flags, NULL);
    }
    else{
        syscall(SYS_wait4, pid, NULL, NULL);
    }
}

void find() // ToDo: Implementeer volgens specificatie.
{
    std::string s;
    std::cout << "Waarop wil je zoeken? " << std::endl;
    std::cin >> s;
    const char* flags_find[] = {"/usr/bin/find", ".", NULL};
    const char* flags_grep[] = {"/bin/grep", s.c_str(), NULL};
    
    int fd[2]; 
    syscall(SYS_pipe, &fd);
    int pid = syscall(SYS_fork);
    if (pid == 0){
        syscall(SYS_close, fd[0]);
        syscall(SYS_dup2, fd[1], 1);
        syscall(SYS_execve, flags_find[0], flags_find, NULL);
    }
    else{
        int pid_2 = syscall(SYS_fork);
        if (pid_2 == 0){
            syscall(SYS_close, fd[1]);
            syscall(SYS_dup2, fd[0], 0);
            syscall(SYS_execve, flags_grep[0], flags_grep, NULL);
        }
        else{
            syscall(SYS_close, fd[1]);
            syscall(SYS_close, fd[0]);
            syscall(SYS_wait4, pid, NULL, NULL);
            syscall(SYS_wait4, pid_2, NULL, NULL);
            std::cin.ignore(INT_MAX, '\n');
        }
        
    }
}
void seek() // ToDo: Implementeer volgens specificatie.
{ //std::cout << "ToDo: Implementeer hier seek()" << std::endl;
    const char* filenames[] = {"seek", "loop"};

    int fd_seek = syscall(SYS_creat, filenames[0], 0644);
    char buff[1] = {'x'};
    syscall(SYS_write, fd_seek, buff, 1);
    syscall(SYS_lseek, fd_seek, 5000000, 1);
    syscall(SYS_write, fd_seek, buff, 1);

    int fd_loop = syscall(SYS_creat, filenames[1], 0644);
    syscall(SYS_write, fd_loop, buff, 1);
    char buffer[1] = {'\0'};
    for (int i = 0; i < 5000000; i++){
        syscall(SYS_write, fd_loop, buffer, 1); 
    }
    syscall(SYS_write, fd_loop, buff, 1);

}


void src() // Voorbeeld: Gebruikt SYS_open en SYS_read om de source van de shell (shell.cc) te printen.
{ int fd = syscall(SYS_open, "shell.cc", O_RDONLY, 0755); // Gebruik de SYS_open call om een bestand te openen.
    char byte[1];                                           // 0755 zorgt dat het bestand de juiste rechten krijgt (leesbaar is).
    while(syscall(SYS_read, fd, byte, 1))                   // Blijf SYS_read herhalen tot het bestand geheel gelezen is,
        std::cout << byte; }                                  //   zet de gelezen byte in "byte" zodat deze geschreven kan worden.
