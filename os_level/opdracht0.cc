#include <iostream>
#include <string>
#include <stdlib.h>
using namespace std;

string ceasar(string line, int argument){
	if (argument < 0){
	    cerr << "Deze applicatie ondersteunde geen negatieve getallen" << endl;
	    exit(1);
	}
	for(unsigned int i=0; i<line.size(); i++){
		if('a' <= line[i] && line[i] <= 'z'){
			line[i]=(((line[i] + argument) - 'a') % 26) + 'a';
			}
		else if('A' <= line[i] && line[i] <= 'Z'){
			line[i]=(((line[i] + argument) - 'A') % 26) + 'A';
			}
               }
	return line;
}


int main(int argc, char *argv[]){
	string line;
	if (argc != 2){
		cerr << "Deze functie heeft exact 1 argument nodig" << endl;
	}
	while(getline(cin, line)){
	    cout << ceasar(line, atoi(argv[1])) << endl;
	}
}

